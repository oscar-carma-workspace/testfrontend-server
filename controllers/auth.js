const { response } = require('express');
const Usuario = require('../models/Usuario');
const bcrypt = require('bcryptjs');
const { generarJWT } = require('../helpers/jwt');

const crearUsuario = async (req, res = response) => {

    // console.log( req.body );
    const { name, email, password } = req.body;
    // console.log(name, email, password);
    
    try {
        // Verificar el email
        const usuario = await Usuario.findOne({ email });
        if ( usuario ) {
            return res.status(400).json({
                oh: false,
                msg: 'El usuario ya existe con ese email'
            })
        }

        // Crear usuario con el modelo
        const dbUsuario = new Usuario( req.body );

        // Hashear la contraseña
        const salt = bcrypt.genSaltSync(3);
        dbUsuario.password = bcrypt.hashSync( password, salt );
        // Generar JWT
        const token = await generarJWT( dbUsuario.id, dbUsuario.name );

        // Crear usuario de DB
        await dbUsuario.save();

        // Generar respuesta exitosa
        return res.status(201).json({
            ok: true,
            uid: dbUsuario.id,
            name,
            token
        });

    } catch (e) {
        console.log(e);
        return res.status(500).json({
            ok: false,
            msg: 'Por favor hable con el administrador'
        });
    }
}

const loginUsuario = async (req, res = response) => {

    // codigo implementado en un middleware
    // const errors = validationResult( req );
    // if( !errors.isEmpty() ){
    //     return res.status(400).json({
    //         ok: false,
    //         errors: errors.mapped()
    //     });
    // }

    const { email, password } = req.body;
    // console.log(email, password);

    try {
        const dbUsuario = await Usuario.findOne({ email });
        if( !dbUsuario ) {
            return res.status(400).json({
                ok: false,
                msg: 'Las credenciales no son validas'
            });
        }

        // Confirmar si el password hace match
        const validPassword = bcrypt.compareSync( password, dbUsuario.password );
        if( !validPassword ) {
            return res.status(400).json({
                ok: false,
                msg: 'Las credenciales no son validas'
            });
        }

        // Generar el JWT
        const token = await generarJWT( dbUsuario.id, dbUsuario.name );

        // Respuesta del servicio
        return res.json({
            ok: true,
            uid: dbUsuario.id,
            name: dbUsuario.name,
            token

        });
    } catch(e) {
        console.log(e);
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador' 
        });
    }
}

const revalidarToken = async (req, res = response) => {

    try {
        const { uid, name } = req;
        const token = await generarJWT( uid, name );

        return res.json({ 
            ok: true,
            uid,
            name,
            token
        });
    } catch(e) {
        console.log(e);
        return res.status(500).json({
            ok: false,
            msg: 'Hble con el administrador'
        });
    }
}

module.exports = {
    crearUsuario,
    loginUsuario,
    revalidarToken
}