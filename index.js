
const express = require('express');
const cors = require('cors');
const { dbConnection } = require('./db/config');
require('dotenv').config();

// console.log( process.env ); Variables de entorno

// Crear el servidor/aplicación de express
const app = express();

// Base de datos
dbConnection();

// Directorio Público
app.use( express.static('public') );

// CORS
app.use( cors() );

// Lectura y parseo del bogy
app.use( express.json() );

// Rutas - middleware
app.use( '/api/auth', require('./routes/auth') );

// Manejar otras rutas
app.get( '*', ( req, res ) => {
    res.sendFile( path.resolve(__dirname, 'public/index.html'));
})

app.listen( process.env.PORT, () => {
    console.log(`Servidor corriendo en el puerto ${ process.env.PORT }`)
});