const mongoose = require("mongoose");

const dbConnection = async() => {
    try {

        await mongoose.connect( process.env.BD_CNN, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });

        console.log('DB Online');

    } catch (e) {
        console.error(e);
        throw new Error('Error a la hora de inicializar la DB');
    }
}

module.exports = {
    dbConnection
}